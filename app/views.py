"""backend code"""
from datetime import datetime, date
import json
from django.shortcuts import render
import boto3
from .forms import AddressForm

IAM_CLIENT = boto3.client("iam")


def create_policy(statements, policy_name, end_date):
    """This function is to create the policy"""

    current_date = date.today()
    condition = {
        "Condition": {
            "DateGreaterThan": {"aws:CurrentTime": str(current_date) + "T00:00:00Z"},
            "DateLessThan": {"aws:CurrentTime": str(end_date.date()) + "T23:59:59Z"},
        }
    }
    message, response = "", ""
    for item in statements:
        if "Condition" in item:
            item["Condition"].update(condition["Condition"])
        else:
            item.update(condition)
    # preparing policy document
    policy_doc = {"Version": "2012-10-17", "Statement": statements}
    try:
        # converting policy document in json format and created policy with
        # name following : <project/practice name>-<project/practice>-dp-<end date>
        response = IAM_CLIENT.create_policy(
            PolicyName=policy_name, PolicyDocument=json.dumps(policy_doc)
        )
    except Exception as error:
        # catch the exception if we get any error in above step
        message = "Error!! " + str(error)
    return message, response


def assign_policy(response, name):
    """ This function is to assign policy to a user"""

    message = ""
    try:
        # assigning the same policy to specified user
        IAM_CLIENT.attach_user_policy(
            PolicyArn=response["Policy"]["Arn"], UserName=name
        )
        message = (
            "Policy : "
            + response["Policy"]["PolicyName"]
            + " is assigned to User : "
            + name
        )
    except Exception as error:
        # catch the exception if we get any error in above step
        message = "Error!! " + str(error)
    return message


def give_access(request):
    """ It will form the policy according to the inputs and assign it to the specified user"""
    # if form is submitted
    if request.method == "POST":
        form = AddressForm(request.POST)
        if form.is_valid():
            service_access = []
            # getting values from frontend
            name = form.cleaned_data["name"]
            account = form.cleaned_data["account"]
            account_name = form.cleaned_data["account_name"]
            service_access = form.cleaned_data.get("service_access")
            end_date = datetime.strptime(form.cleaned_data["end_date"], "%Y-%m-%d")
            users, message = [], ""
            all_users_details = IAM_CLIENT.list_users(MaxItems=300)
            for user_detail in all_users_details["Users"]:
                users.append(user_detail["UserName"])
            # checking user is exist or not
            if name in users:
                # getting the documents for all policy_arn in service_access
                # and aggregate all in "statements" list
                statements = []
                for arn in service_access:
                    policy = IAM_CLIENT.get_policy(PolicyArn=arn)
                    statement = IAM_CLIENT.get_policy_version(
                        PolicyArn=arn, VersionId=policy["Policy"]["DefaultVersionId"]
                    )["PolicyVersion"]["Document"]["Statement"]
                    statements = statements + statement
                # calling function to create policy
                policy_result = create_policy(
                    statements,
                    account_name
                    + "-"
                    + account
                    + "-dp-"
                    + str(end_date.day)
                    + str(end_date.month)
                    + str(end_date.year),
                    end_date,
                )
                if not policy_result[0]:
                    # calling function to assign policy to a user
                    assign_result = assign_policy(policy_result[1], name)
                    message = assign_result
                else:
                    message = policy_result[0]
            else:
                message = "user does not exist"
        return render(request, "result.html", {"message": message})
    else:
        form = AddressForm()
    # returning form
    return render(request, "AWS_form.html", {"form": form})
