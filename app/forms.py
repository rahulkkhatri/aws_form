"""form input fields"""
from django import forms

# creating our forms
class AddressForm(forms.Form):
    """ AWS_form.html will get the input fields from here and here the values will be stored."""

    access_types = (
        ("arn:aws:iam::aws:policy/AmazonEC2FullAccess", "AmazonEC2FullAccess"),
        ("arn:aws:iam::aws:policy/AmazonS3FullAccess", "AmazonS3FullAccess"),
        ("arn:aws:iam::aws:policy/AmazonEKSServicePolicy", "AmazonEKSServicePolicy"),
    )
    accounts = (("project", "Delivery"), ("practice", "Training"))
    name = forms.EmailField(
        label="Username",
        widget=forms.TextInput(attrs={"placeholder": "example@infostretch.com"}),
    )
    account = forms.ChoiceField(label="Account", choices=accounts)
    account_name = forms.CharField(label="Project/Practice Name", max_length=100)
    service_access = forms.MultipleChoiceField(
        label="Service Access",
        choices=access_types,
        widget=forms.CheckboxSelectMultiple(),
    )
    end_date = forms.CharField(
        label="End Date",
        max_length=10,
        widget=forms.TextInput(attrs={"placeholder": "YYYY-MM-DD"}),
    )
